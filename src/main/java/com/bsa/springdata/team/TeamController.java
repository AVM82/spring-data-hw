package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/team")

public class TeamController {

    @Autowired
    TeamService teamService;


    @GetMapping("/techCount/{tech}")
    public int techCount(@PathVariable String tech) {
        return teamService.techCount(tech);
    }

}
