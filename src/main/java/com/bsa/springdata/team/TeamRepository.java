package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Query("select count(t) from Team t where t.technology.name = :tech")
    int countByTechnologyName(String tech);

    @Transactional
    @Modifying
    @Query("update Team t set t.technology = :tech where t.id = :teamId")
    void updateTechnology(UUID teamId, Technology tech);

    @Query("select t from Team t where t.users.size < :devsNumber and t.technology.name = :oldTechnologyName")
    List<Team> findAllByTechNameAndUsersCount(int devsNumber, String oldTechnologyName);

    @Query("select t from Team t where t.name = :teamName")
    Optional<Team> findByName(String teamName);

    @Transactional
    @Modifying
    @Query(value = "update teams t set name = concat(t.name,'_', p.name, '_', tech.name) " +
            "from projects p, technologies tech " +
            "where t.name = :hipsters and p.id = t.project_id and tech.id = t.technology_id", nativeQuery = true)
    void normalizeName(String hipsters);
}
