package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return userService.getUserById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
    }

    @PostMapping
    public UUID createUser(@RequestBody CreateUserDto user) {
        return userService.createUser(user)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create user."));
    }

    @GetMapping("/lastname/{lastName}")
    public List<UserDto> findByLastName(@PathVariable String lastName,
                                        @RequestParam(defaultValue = "0") int page,
                                        @RequestParam(defaultValue = "10") int size
    ) {
        return userService.findByLastName(lastName, page, size);
    }

    @GetMapping("/city/{city}")
    public List<UserDto> findUserByCity(@PathVariable String city) {
        return userService.findByCity(city);
    }

    @GetMapping("/experience/{experience}")
    public List<UserDto> findByExperience(@PathVariable int experience) {
        return userService.findByExperience(experience);
    }

    @GetMapping("/find-by-city-and-room")
    public List<UserDto> findByRoomAndCity(@RequestParam String city, @RequestParam String room) {
        return userService.findByRoomAndCity(city, room);
    }

    @DeleteMapping("/delete-by-experience/{experience}")
    public int deleteByExperience(@PathVariable int experience) {
        return userService.deleteByExperience(experience);
    }
}
