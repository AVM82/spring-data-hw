package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select DISTINCT u.office from User u where u.team.technology.name = :technology")
    List<Office> findByTechnology(String technology);

    @Transactional
    @Modifying
    @Query("update Office o set o.address = :newAddress where o.users.size > 0 and o.address = :oldAddress")
    void updateAddress(String oldAddress, String newAddress);

    Office findByAddress(String address);
}
