package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/project")
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @GetMapping("/findTop5ByTechnology/{tech}")
    public List<ProjectDto> findTop5ByTechnology(@PathVariable String tech) {
        return projectService.findTop5ByTechnology(tech);
    }
}
