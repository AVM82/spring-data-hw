package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(value = " select t1.name, t1.description, t1.id from " +
            "(select p2.name, p2.description, p2.id, count(*) as on_project " +
            "from ((users u inner join teams t on u.team_id = t.id) " +
            "inner join technologies tech on tech.id = t.technology_id and tech.name = :tech) " +
            "inner join projects p2 on t.project_id = p2.id " +
            "group by p2.name, p2.description, p2.id order by on_project) as t1 limit 5 ", nativeQuery = true)
    List<Project> findTop5ByTechnology(String tech);

    @Query(value = "select count (distinct t.project_id) from " +
            "teams t inner join " +
            "(users u inner join " +
            "(user2role u2r inner join roles r on u2r.role_id = r.id and r.name = 'Developer') as t1 " +
            "on t1.user_id = u.id) as t2 " +
            "on t.id = t2.team_id", nativeQuery = true)
    int getCountWithRole(String role);

    @Query(value = "select p.id, p.name, p.description, " +
            "(select count(*) from teams t2 where t2.project_id = p.id ) as teamsNumber, " +
            "(select count(*) from  users u2, teams t3 where u2.team_id = t3.id and t3.project_id = p.id)  as developersNumber " +
            "from projects p " +
            "order by teamsNumber desc, developersNumber desc, p.name desc " +
            "limit 1", nativeQuery = true)
    Project findBiggest();

    @Query(value = "select p.name, " +
            "(select count(*) from teams t2 where t2.project_id = p.id ) as teamsNumber, " +
            "(select count(*) from  users u2, teams t3 where u2.team_id = t3.id and t3.project_id = p.id)  as developersNumber, " +
            "(select string_agg(t.name,',') from technologies t, teams t4 where t.id = t4.technology_id and t4.project_id = p.id) as Technologies " +
            "from projects p " +
            "order by p.name ", nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}