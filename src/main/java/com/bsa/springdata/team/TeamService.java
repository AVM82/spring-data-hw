package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) throws Exception {
        List<Team> teams = teamRepository.findAllByTechNameAndUsersCount(devsNumber, oldTechnologyName);
        for (Team team : teams) {
            Technology tech = technologyRepository.findByName(newTechnologyName).orElseThrow(Exception::new);
            teamRepository.updateTechnology(
                    team.getId(),
                    tech);
        }
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }

    public int techCount(String tech) {
        return teamRepository.countByTechnologyName(tech);
    }
}
