package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfficeService {

    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository.findByTechnology(technology)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        try {
            officeRepository.updateAddress(oldAddress, newAddress);
            var result = officeRepository.findByAddress(newAddress);
            return Optional.of(OfficeDto.fromEntity(result));

        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
