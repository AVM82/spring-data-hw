package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology).stream().map(ProjectDto::fromEntity).collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        try {
            var result = ProjectDto.fromEntity(projectRepository.findBiggest());

            return Optional.of(result);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        try {
            var tech = Technology.builder()
                    .link(createProjectRequest.getTechLink())
                    .name(createProjectRequest.getTech())
                    .description(createProjectRequest.getTechDescription())
                    .build();

            var project = Project.builder()
                    .name(createProjectRequest.getProjectName())
                    .description(createProjectRequest.getProjectDescription())
                    .build();


            var team = Team.builder()
                    .room(createProjectRequest.getTeamRoom())
                    .name(createProjectRequest.getTeamName())
                    .area(createProjectRequest.getTeamArea())
                    .technology(technologyRepository.save(tech))
                    .project(projectRepository.save(project))
                    .build();

            var result = teamRepository.save(team);
            return result.getId();
        } catch (Exception e) {
            return null;
        }
    }
}
