package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findUserByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = :city order by u.lastName asc ")
    List<User> findAllBuCity(String city);

    List<User> findUserByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findByRoomAndCity(String city, String room, Sort sort);

    int deleteByExperienceLessThan(int experience);
}
